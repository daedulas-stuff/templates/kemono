package com.example.kemonogame

import kotlinx.serialization.Serializable
import me.derfruhling.kemono.Game
import me.derfruhling.kemono.graphics.RenderService
import me.derfruhling.kemono.graphics.interfaces.Texture
import me.derfruhling.kemono.graphics.ui.Screen
import me.derfruhling.kemono.input.InputTrigger
import me.derfruhling.kemono.input.sources.WindowGamepadButtonInputSource
import me.derfruhling.kemono.input.sources.WindowGamepadLeftAxisSnappedInputSource
import me.derfruhling.kemono.input.sources.WindowKeyboardInputSource
import me.derfruhling.kemono.input.sources.WindowKeyboardVectorInputSource
import me.derfruhling.kemono.map.*
import me.derfruhling.kemono.map.entities.SpriteEntity
import me.derfruhling.kemono.math.*
import me.derfruhling.kemono.platform.OperatingSystem
import me.derfruhling.kemono.savedata.SaveDataCodec
import me.derfruhling.kemono.savedata.SaveDataFile
import me.derfruhling.kemono.services.Component
import me.derfruhling.kemono.services.service
import me.derfruhling.kemono.text.LocaleService
import me.derfruhling.kemono.text.TextService
import me.derfruhling.quantum.core.log.logger

class TestGame(@Suppress("UNUSED_PARAMETER") args: List<String>) : Game() {
    private val log by logger

    // FIXME replace with game name in various cases
    override val referenceName: String = when {
        OperatingSystem.isWindows -> "KemonoTemplate"
        OperatingSystem.isLinux -> "kemono-template"
        OperatingSystem.isMacOS -> "Kemono Template"
        else -> "kemono-template" // default to something acceptable
    }

    @Serializable
    data class TestSaveData(
        var x: Float = 0.0f,
        var y: Float = 0.0f
    )

    private val grass = Tile.Oriented("grass", 0, 0, 0)

    private val movementInput by input.vectorInputAction(
        xTriggers = listOf(
            InputTrigger.KeyboardAxis.X,
            InputTrigger.GamepadAxis.LEFT_X
        ),
        yTriggers = listOf(
            InputTrigger.KeyboardAxis.Y,
            InputTrigger.GamepadAxis.LEFT_Y
        )
    )

    private val testInput by input.buttonInputAction(
        InputTrigger.Key.SPACE
    )

    override suspend fun initialize() {
        super.initialize()

        platform.primaryWindow.title = "test game"
        locale.loadArbitraryLocale("kemono:lang/km_info.lang")

        saveData.registerSaveDataCodec(SaveDataCodec.fromSerializer(TestSaveData.serializer(), ::TestSaveData))

        map.registerTileSpritesheet(0, "/textures/tilemap0.tex", Vector2(16, 16))

        map.tileSet = MutableTileSet()
            .withTiles(grass)
            .build()

        input.addButtonSource(WindowKeyboardInputSource)
        input.addButtonSource(WindowGamepadButtonInputSource)
        input.addAxisSource(WindowGamepadLeftAxisSnappedInputSource)
        input.addAxisSource(WindowKeyboardVectorInputSource::wasd)

        val currentSave = saveData.getSave<TestSaveData>(SaveDataFile.named("save-test"))
        map.setMap(Map(map, MapSource.Null, Vector2(currentSave.x, currentSave.y), Direction.NORTH).apply {
            loadChunk(Vector2(0, 0), explicit = false).fill(grass.borderTopRight)
            loadChunk(Vector2(-1, 0), explicit = false).fill(grass.borderTopLeft)
            loadChunk(Vector2(0, -1), explicit = false).fill(grass.borderBottomRight)
            loadChunk(Vector2(-1, -1), explicit = false).fill(grass.borderBottomLeft)

            insertEntity(TestEntity(1, Vector2(2f, 2f), Direction.NORTH))
        })
    }

    override suspend fun globalUpdate() {
        super.globalUpdate()

        map.currentMap!!.playerEntity.position += (movementInput.state.value / Vector2(10.0f,10.0f)) *
                lastFrameTimeAvg

        if(testInput.state.value && currentScreen == null) {
            async.runOnRenderThread { currentScreen = TestScreen() }
        } else if(!testInput.state.value && currentScreen != null) {
            async.runOnRenderThread {
                currentScreen?.dispose()
                currentScreen = null
            }
        }
    }

    override suspend fun tearDown() {
        saveData.getSave<TestSaveData>(SaveDataFile.named("save-test")).apply {
            map.currentMap!!.playerEntity.let { entity ->
                x = entity.position.x
                y = entity.position.y
            }
        }

        super.tearDown()
    }

    private class TestEntity(
        override val id: Int,
        override var position: Vector2<Float>,
        override var direction: Direction
    ) : SpriteEntity(), Component {
        private val renderService: RenderService by service()
        private val textService: TextService by service()
        private val textureRef = renderService.texturePool["kemono:textures/fonts/km_mono.tex"]

        override val size: Vector2<Float> = Vector2(1f, 1f)
        override val originOffset: Vector2<Float> = Vector2(0.5f, 0.5f)
        override val texture: Texture get() = textureRef.value

        override val textureCorners: Corners<Vector2<Float>>
            get() = textService.monospaceFont.mapCharacter('A')
                ?: textService.monospaceFont.mapCharacter('?')!!

        override fun dispose() {
            super.dispose()
            textureRef.dispose()
        }
    }

    private class TestScreen : Screen(), Component {
        private val localeService: LocaleService by service()

        val text = text("${localeService["km.name"]} ${localeService["km.version"]}")

        override fun render() {
            text.render()
        }

        override suspend fun update() {}

        override fun dispose() {
            text.dispose()
        }
    }
}