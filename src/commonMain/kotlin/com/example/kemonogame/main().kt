package com.example.kemonogame

import me.derfruhling.kemono.runGame

fun main(args: Array<String>) = runGame(args, ::TestGame)
