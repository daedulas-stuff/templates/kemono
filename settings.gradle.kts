pluginManagement {
    repositories {
        mavenCentral()
        gradlePluginPortal()
        maven("https://gitlab.com/api/v4/projects/50850114/packages/maven")
    }
}

plugins {
    id("org.gradle.toolchains.foojay-resolver-convention") version "0.5.0"
}

rootProject.name = "kemono-template"