import me.derfruhling.kemono.gradle.CompileResources
import me.derfruhling.kemono.gradle.withLinuxX64Libraries

plugins {
    kotlin("multiplatform") version "1.9.10"
    kotlin("plugin.serialization") version "1.9.10"
    id("me.derfruhling.kemono.kebuild") version "2023.1a2"
}

group = "com.example"
version = "1.0-SNAPSHOT"

val os = System.getProperty("os.name").lowercase()
val mainClass: String by properties

repositories {
    mavenCentral()

    // quantum core
    maven("https://gitlab.com/api/v4/projects/47184198/packages/maven")

    // kemono
    maven("https://gitlab.com/api/v4/projects/50850114/packages/maven")
}

dependencies {
    resources("me.derfruhling.kemono:kemono-resources:2023.1a2")
}

kotlin {
    mingwX64 {
        binaries {
            executable {
                entryPoint(mainClass)
            }
        }
    }

    linuxX64 {
        binaries {
            executable {
                entryPoint(mainClass)
            }
        }

        withLinuxX64Libraries()
    }

    sourceSets {
        val commonMain by getting {
            dependencies {
                implementation("me.derfruhling.quantum:quantum-core:1.0.0-SNAPSHOT")
                implementation("me.derfruhling.kemono:kemono:2023.1a2")
            }
        }

        val commonTest by getting {
            dependencies {
                implementation(kotlin("test"))
            }
        }

        val nativeMain by creating { dependsOn(commonMain) }
        val nativeTest by creating { dependsOn(commonTest) }
        val mingwX64Main by getting { dependsOn(nativeMain) }
        val mingwX64Test by getting { dependsOn(nativeTest) }
        val linuxX64Main by getting { dependsOn(nativeMain) }
        val linuxX64Test by getting { dependsOn(nativeTest) }
    }
}

tasks {
    val compileKmResources by creating(CompileResources::class) {
        dependsOn("metadataCommonMainProcessResources")

        sourceDir.set(
            buildDir
                .resolve("processedResources")
                .resolve("metadata")
                .resolve("commonMain")
        )

        outputDir.set(
            buildDir
                .resolve("compiledResources")
                .resolve("common")
        )
    }

    val resolveResourceDependencies by creating {
        doFirst {
            val deps = configurations.resources.get().dependencies
            deps.forEach {
                println(it)
            }

            println("Existing files: " + configurations.resources.get().files.joinToString())
            println("Resolved files: " + configurations.resources.get().resolve().joinToString())
            println("Artifacts: " + configurations.resources.get().allArtifacts.joinToString())
        }
    }

    when {
        os.contains("nix") || os.contains("nux") -> {
            val linkDebugExecutableLinuxX64 by getting
            val linkReleaseExecutableLinuxX64 by getting
            val linuxX64ProcessResources by getting

            val stageDebug by creating(Sync::class) {
                doFirst { mkdir(buildDir.resolve("staged/linuxX64/debug")) }

                dependsOn(
                    linkDebugExecutableLinuxX64,
                    compileKmResources
                )

                from(linkDebugExecutableLinuxX64)

                into(buildDir.resolve("staged/linuxX64/debug"))

                from(compileKmResources) { into("resources") }
                from(provider {
                    configurations.resources.get().resolvedConfiguration.files.map {
                        zipTree(it)
                    }
                }) { into("resources") }
            }

            val runStagedDebug by creating(Exec::class) {
                dependsOn(stageDebug)
                workingDir(buildDir.resolve("staged/linuxX64/debug"))
                executable(buildDir.resolve("staged/linuxX64/debug/${project.name}.kexe"))
                args(
                    (findProperty("kemonoArgs") as String?)
                        ?.split(',')
                        ?.toList() ?: emptyList<String>()
                )
            }

            val stageRelease by creating(Sync::class) {
                doFirst { mkdir(buildDir.resolve("staged/linuxX64/release")) }

                dependsOn(
                    linkReleaseExecutableLinuxX64,
                    compileKmResources
                )

                from(
                    linkReleaseExecutableLinuxX64,
                    rootDir.resolve("vcpkg_installed/linuxX64/x64-linux/bin")
                        .listFiles { _, name -> name.endsWith(".so") }
                )

                into(buildDir.resolve("staged/linuxX64/release"))

                from(compileKmResources) { into("resources") }
                from(provider {
                    configurations.resources.get().resolvedConfiguration.files.map {
                        zipTree(it)
                    }
                }) { into("resources") }
            }

            val runStagedRelease by creating(Exec::class) {
                dependsOn(stageDebug)
                workingDir(buildDir.resolve("staged/linuxX64/release"))
                executable(buildDir.resolve("staged/linuxX64/release/${project.name}.kexe"))
                args(
                    (findProperty("kemonoArgs") as String?)
                        ?.split(',')
                        ?.toList() ?: emptyList<String>()
                )
            }
        }

        os.contains("windows") -> {
            val linkDebugExecutableMingwX64 by getting
            val linkReleaseExecutableMingwX64 by getting
            val mingwX64ProcessResources by getting

            val stageDebug by creating(Sync::class) {
                doFirst { mkdir(buildDir.resolve("staged/mingwX64/debug")) }

                dependsOn(
                    linkDebugExecutableMingwX64,
                    compileKmResources
                )

                from(linkDebugExecutableMingwX64)

                into(buildDir.resolve("staged/mingwX64/debug"))

                from(compileKmResources) { into("resources") }
                from(provider {
                    configurations.resources.get().resolvedConfiguration.files.map {
                        zipTree(it)
                    }
                }) { into("resources") }
            }

            val runStagedDebug by creating(Exec::class) {
                dependsOn(stageDebug)
                workingDir(buildDir.resolve("staged/mingwX64/debug"))
                executable(buildDir.resolve("staged/mingwX64/debug/${project.name}.exe"))
                args(
                    (findProperty("kemonoArgs") as String?)
                        ?.split(',')
                        ?.toList() ?: emptyList<String>()
                )
            }

            val stageRelease by creating(Sync::class) {
                doFirst { mkdir(buildDir.resolve("staged/mingwX64/release")) }

                dependsOn(
                    linkReleaseExecutableMingwX64,
                    compileKmResources
                )

                from(linkReleaseExecutableMingwX64)

                into(buildDir.resolve("staged/mingwX64/release"))

                from(compileKmResources) { into("resources") }
                from(provider {
                    configurations.resources.get().resolvedConfiguration.files.map {
                        zipTree(it)
                    }
                }) { into("resources") }
            }

            val runStagedRelease by creating(Exec::class) {
                dependsOn(stageRelease)
                workingDir(buildDir.resolve("staged/mingwX64/release"))
                executable(buildDir.resolve("staged/mingwX64/release/${project.name}.exe"))
                args(
                    (findProperty("kemonoArgs") as String?)
                        ?.split(',')
                        ?.toList() ?: emptyList<String>()
                )
            }
        }

        else -> {
            throw IllegalStateException("Unsupported OS $os")
        }
    }
}
