# kemono
_a game engine for making pixels_

This version of the template builds on kemono version 
[`2023.1α2`](https://gitlab.com/daedulas-stuff/kemono/-/releases/2023.1a2).

See [kemono's repository](https://gitlab.com/daedulas-stuff/kemono).

You can fork this repository for a quick start with kemono.
You can [unlink the fork](https://docs.gitlab.com/ee/user/project/repository/forking_workflow.html#unlink-a-fork) 
in the settings to make it less forkly.

## A fair word of warning

kemono is in an αlpha state.
This means that there will be tons of things that are broken or just plain
missing, but could be fixed or added later.

## help how do i run the game

```bash
./gradlew runStagedDebug
```

Since resources are not bundled into the executable itself, you must run the
executable inside a stage.
Otherwise, it will not find the resources.

## Building Releases

```bash
./gradlew stageRelease
```

This puts the release version in `build/staged/<your-platform>/release`.
You can pack this up and distribute it, but you should probably test it first! 
